# Celduin Glossary

| Term | Definition |
| ---- | ---------- |
| bst | BuildStream (prefered), buildstream |
| [bst-plugins-experimental](https://buildstream.gitlab.io/bst-plugins-experimental/) | External plugins for bst 2 |
| bst-external | External plugins for bst <= 1.4 |
| BSP | Board Support Package. Hardware vendors often release complete software packages to support their boards, which is sometimes referred to as a Board Support Package. Alternative terms: Product Development Kit (PDK) |
| REAPI | Remote Execution API |

# Architectures
There are lots of confusingly interchangeable terms when referring to
Instruction Set Architecture. This table lists our preferred terms and some
alternatives you might see in the wild. We prefer to use industry standard terms
so that these tools are less surprising.

| Architecture (preferred) | AKA | Definition |
| --- | --- | --- |
| ISA | arch, architecture | Instruction Set Architecture |
| x86 | ia32, i686, i586, i486, i386 | 32-bit x86 architecture by Intel. There are several generations: i386 is the first 32-bit arch, followed by i486, i586, i686 etc |
| x86-64 | x86_64, x64, AMD64, Intel 64 | 64-bit extension of the x86 instruction set. The original spec was created by AMD, hence AMD64 |
| aarch32 | Aarch32, arm32, arm | 32-bit [ARM Architecture](https://developer.arm.com/architectures/cpu-architecture/a-profile) |
| aarch64 | Aarch64, arm64 | 64-bit version of the ARM architecture |

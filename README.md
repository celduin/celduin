# Celduin

## Table of contents

- [Introduction](#Introduction)
- [Getting Started with BuildStream](#getting-started-with-buildstream)
- [Contact us](#contact-us)

## Introduction

The Celduin project tackles the problems involved with building and testing software at scale.

We heavily leverage clients and servers that use and implement the remote execution API (REAPI).

Currently Celduin is split into 4 workstreams.

### Remote execution

This focuses on the end-to-end deployment of production-ready REAPI servers and supporting services. The aim is to provide implementations that are completely agnostic to the REAPI client used.

[Buildbarn](https://github.com/buildbarn) is used as the REAPI server implementation.

The main entrypoint for this workstream can be found at [celduin-infra](https://gitlab.com/celduin/infrastructure/celduin-infra).

### BOARD

This focuses on the use of [BuildStream](https://buildstream.build/) to deploy full software stacks to hardware devices.

Minimal definitions for software support are provided that integrate with existing BuildStream projects such as [freedesktop-sdk](https://gitlab.com/freedesktop-sdk/freedesktop-sdk), [gnome-build-meta](https://gitlab.gnome.org/GNOME/gnome-build-meta), [CRASH](https://gitlab.com/celduin/crash) and [libreML](https://gitlab.com/libreml/libreml).

The main entrypoint for this workstream can be found at [https://gitlab.com/celduin/bsps](https://gitlab.com/celduin/bsps).

### BuildStream-Bazel

Bazel and BuildStream have complementary strengths. BuildStream is able to construct whole systems in multiple output formats. Bazel is able to perform fast incremental builds for a particular application in an existing environment.

We want Bazel and BuildStream developers to be able to leverage the strengths of both tools. We support this through the development of Bazel rules for BuildStream and Bazel plugins for BuildStream.

There is also a [talk](https://www.youtube.com/watch?v=21VPe7HcuPE) by Daniel Silverstone at BazelCon 2019 which discusses the motivation for interoperability in more detail.

The main entrypoint for this workstream is the [roadmap](https://gitlab.com/celduin/buildstream-bazel/issue-tracker/-/blob/master/roadmap.md).

### Repository and Change Mangement (RCM)

We investigate CI/CD systems such as [zuul](https://zuul-ci.org/docs/zuul/) that aim to manage repository changes (Changesets, PRs, MRs) in complex multi-repository scenarios where a high number of developers are involved.

The main entrypoint for this workstream can be found at [https://gitlab.com/celduin/repository-change-management](https://gitlab.com/celduin/repository-change-management).

## Getting started with BuildStream

Important terms and acronyms are defined in the [Celduin Glossary](celduin-glossary).

### Review main projects

There are three main projects you should be interested in when starting to work with BuildStream:
- BuildStream itself (the integration tool)
  - https://buildstream.build/
  - https://buildstream.build/install.html
  - https://docs.buildstream.build/
  - https://gitlab.com/BuildStream/buildstream
- freedesktop-sdk, A minimal Linux runtime. Almost all BuildStream projects use freedesktop-sdk to construct build-time and run-time software.
  - https://gitlab.com/freedesktop-sdk/freedesktop-sdk
- gnome-build-meta, GNOME release team uses BuildStream to manage meta-data for their software stack (you can build GNOME OS for Pinebook Pro, for example)
  - https://gitlab.gnome.org/GNOME/gnome-build-meta


### Using BuildStream

The Celduin project uses development versions of BuildStream wherever possible, to support development efforts as BuildStream progresses towards a [v2.0 release](https://mail.gnome.org/archives/buildstream-list/2020-April/msg00013.html)

The current BuildStream development tag that we use is [1.93.1](https://gitlab.com/BuildStream/buildstream/-/tags/1.93.1)

Docker images for known working versions of 1.93.1 (produced with the [freedesktop-sdk-docker-images](https://gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images) project) can be found with:
  - `registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/bst2/amd64:dev-cphang-bst-1-93-1`
    - This contains a backport for https://gitlab.com/BuildStream/buildstream/-/merge_requests/1926, that closes the critical issue described at https://gitlab.com/BuildStream/buildstream/-/issues/1319 and is built via this [branch](https://gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/-/tree/cphang/bst-1-93-1)
  - `registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/bst2/arm64:71f830006f10ea920490e4da66e8df9615a17a3d` if you want to use 1.93.1 free of backports.

To consume these docker images, you can use the utility script [`bst-here`](https://buildstream.build/container_install.html)

```shell
bst-here -i registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/bst2/amd64:dev-cphang-bst-1-93-1
```

## Contact us

You can join us on IRC at #celduin on freenode (freenode.net).

Also join the following IRC channels:
- irc.gnome.org/#buildstream
- irc.freenode.net/#freedesktop-sdk
- irc.gnome.org/#gnome-os

Subscribe to the following mailing lists:
- https://mail.gnome.org/mailman/listinfo/buildstream-list
- https://lists.freedesktop.org/mailman/listinfo/freedesktop-sdk
